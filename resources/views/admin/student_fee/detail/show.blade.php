<div class="card shadow">
    <div class="card-header">
        <div class="row">
            <div class="col-sm-8">
                <h2 class="m-2 font-weight-bold float-left text-primary">Name: {{ $data['student']->students->student_name }}</h2>
                <h2 class="m-2 font-weight-bold float-left text-primary"> Roll No: #{{ $data['student']->students->student_roll_no }}</h2>
                <h2 class="m-2 font-weight-bold float-left text-primary">Total Fee: Rs.{{ $data['student']->student_fee }}</h2>
                <h2 class="m-2 font-weight-bold float-left text-primary">Paid fee: Rs.{{ $data['student']->paid_fee }}</h2>
                <h2 class="m-2 font-weight-bold float-left text-primary">Remaining Fee: Rs.{{ $data['student']->remaining_fee }}</h2>
            </div>
            <div class="col-sm-4">
                <div class="float-right">
                    <button class="btn btn-primary btn-print">Print</button>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-sm" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Student</td>
                        <th>Roll No</th>
                        <th>Class & section</th>
                        <th>Invoice Number</th>
                        <th>Fee Amount</th>
                        <th>Fee's Month</th>
                        <th>Paid date</th>
                        <th class="noprint">Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Student</td>
                        <th>Roll No</th>
                        <th>Class & section</th>
                        <th>Invoice Number</th>
                        <th>Fee Amount</th>
                        <th>Fee's Month</th>
                        <th>Paid date</th>
                        <th class="noprint">Action</th>
                    </tr>
                </tfoot>
                <tbody>
                   @foreach($data['fee'] as $key => $fee)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $fee->student_fees->students->student_name }}</td>
                            <td>#{{ $fee->student_fees->students->student_roll_no }}</td>
                            <td>{{ $fee->student_fees->students->class->class_title }} | {{ $fee->student_fees->students->class->section_name }} </td>
                            <td>{{$fee->invoice_number}}</td>
                            <td>Rs.{{ $fee->fee_amount }}</td>
                            <td>{{ \Carbon\Carbon::parse($fee->fee_of_month)->toFormattedDateString()}} </td>
                            <td>{{ $fee->paid_date }}</td>
                            <td class="noprint">
                                @if($fee->paid_date == '')
                                    <a href="print_fee_voucher?voucher={{$fee->id}}" class="btn btn-primary print-fee-voucher" >Print Slip</a>
                                @else
                                @endif
                                <button class="btn btn-success btn-edit" data-id="{{ $fee->id }}">Edit</button>
                            </td>
                        </tr>
                   @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer"></div>
</div>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.22/datatables.min.js"></script>

<script>
    $(document).ready(function () {
        $('#dataTable').DataTable();
    });
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>