@extends('student.layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        <h2 class="h2 float-left">Fee Voucher</h2>
        <button class="btn btn-primary btn-print float-right">Print</button>
    </div>
    <div class="card-body">
        <div class="row fee-voucher" id="feeVoucher">
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-sm-10 m-auto">
                        <h1>The Grammer School</h1>
                    </div>
                    <div class="col-sm-12 m-auto">
                        <h3>Parent Copy</h3>
                    </div>
                    <div class="col-sm-10 ml-auto">
                        <h4>Last Date: {{ Carbon\Carbon::createFromDate(null, null, 10)->format('jS F Y') }}</h4>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="heading">Invoice No:</span>
                            </div>
                            <div class="col-sm-6">
                                <h4>{{ $data->invoice_number }}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="heading">Name: </span>
                            </div>
                            <div class="col-sm-6">
                                <h4>{{ $data->student_fees->students->student_name }}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="heading">Father Name: </span>
                            </div>
                            <div class="col-sm-6">
                                <h4>{{ $data->student_fees->students->student_father_name }}</h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="heading">Class: </span>
                            </div>
                            <div class="col-sm-6">
                                <h4>{{ $data->student_fees->students->class->class_title }} |
                                    {{ $data->student_fees->students->class->section_name }}</h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="heading">Fee of Month: </span>
                            </div>
                            <div class="col-sm-6">
                                <h4>{{ Carbon\Carbon::parse($data->fee_of_month)->format('F Y') }}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <table class="table table-bordered text-center">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Fee </th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td colspan="2">Total</td>
                                    <td>Rs.{{ $data->fee_amount }}</td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>{{ Carbon\Carbon::parse($data->fee_of_month)->format('F Y') }}</td>
                                    <td>Rs.{{ $data->fee_amount }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-sm-8 m-auto">
                        <span>Fine charge 100 rupees per day after due date.</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-sm-10 m-auto">
                        <h1>The Grammer School</h1>
                    </div>
                    <div class="col-sm-12 m-auto">
                        <h3>Bank Copy</h3>
                    </div>
                    <div class="col-sm-10 ml-auto">
                        <h4>Last Date: {{ Carbon\Carbon::createFromDate(null, null, 10)->format('jS F Y') }}</h4>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="heading">Invoice No:</span>
                            </div>
                            <div class="col-sm-6">
                                <h4>{{ $data->invoice_number }}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="heading">Name: </span>
                            </div>
                            <div class="col-sm-6">
                                <h4>{{ $data->student_fees->students->student_name }}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="heading">Father Name: </span>
                            </div>
                            <div class="col-sm-6">
                                <h4>{{ $data->student_fees->students->student_father_name }}</h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="heading">Class: </span>
                            </div>
                            <div class="col-sm-6">
                                <h4>{{ $data->student_fees->students->class->class_title }} |
                                    {{ $data->student_fees->students->class->section_name }}</h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="heading">Fee of Month: </span>
                            </div>
                            <div class="col-sm-6">
                                <h4>{{ Carbon\Carbon::parse($data->fee_of_month)->format('F Y') }}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <table class="table table-bordered text-center">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Fee </th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td colspan="2">Total</td>
                                    <td>Rs. {{ $data->fee_amount }}</td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>{{ Carbon\Carbon::parse($data->fee_of_month)->format('F Y') }}</td>
                                    <td>Rs.{{ $data->fee_amount }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-sm-8 m-auto">
                        <span>Fine charge 100 rupees per day after due date.</span>
                    </div>
                </div>

            </div>

            <div class="col-sm-4">
                <div class="row">
                    <div class="col-sm-10 m-auto">
                        <h1>The Grammer School</h1>
                    </div>
                    <div class="col-sm-12 m-auto">
                        <h3>Office Copy</h3>
                    </div>
                    <div class="col-sm-10 ml-auto">
                        <h4>Last Date: {{ Carbon\Carbon::createFromDate(null, null, 10)->format('jS F Y') }}</h4>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="heading">Invoice No:</span>
                            </div>
                            <div class="col-sm-6">
                                <h4>{{ $data->invoice_number }}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="heading">Name: </span>
                            </div>
                            <div class="col-sm-6">
                                <h4>{{ $data->student_fees->students->student_name }}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="heading">Father Name: </span>
                            </div>
                            <div class="col-sm-6">
                                <h4>{{ $data->student_fees->students->student_father_name }}</h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="heading">Class: </span>
                            </div>
                            <div class="col-sm-6">
                                <h4>{{ $data->student_fees->students->class->class_title }} |
                                    {{ $data->student_fees->students->class->section_name }} </h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <span class="heading">Fee of Month: </span>
                            </div>
                            <div class="col-sm-6">
                                <h4>{{ Carbon\Carbon::parse($data->fee_of_month)->format('F Y') }} </h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <table class="table table-bordered text-center">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Fee </th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td colspan="2">Total</td>
                                    <td>Rs. {{ $data->fee_amount }}</td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>{{ Carbon\Carbon::parse($data->fee_of_month)->format('F Y') }}</td>
                                    <td>Rs.{{ $data->fee_amount }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-sm-8 m-auto">
                        <span>Fine charge 100 rupees per day after due date.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script')
<script>
    $(document).ready(function(){
        $(".btn-print").on('click', function(){
            $("#feeVoucher").printThis();
        });
    });
</script>
@endsection