@extends('teacher.layouts.app')


@section('content')

@isset($data)

<div class="classAttendance-Page">

    <div class="ClassInfo">
        <label>Class:</label> <span>{{ $data['classSection']->class_title }}</span>
        <label>Section:</label> <span>{{ $data['classSection']->section_name }}</span>
        <label>Total Student:</label> <span>{{ $data['classSection']->seats }}</span>

        <label>Enroll Student:</label> <span>{{ $data['enrollStudent'] }}</span>
    </div>

    <div class="card shadow">
        <div class="card-header py-4">

        </div>
        <div class="card-body">
            <form id="mark_student_attendance">
                @csrf
                <div id="markAttendanceMessage"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-sm " id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th style="width: 1rem;">#</th>
                                <th style="width: 7rem;">Student</th>
                                <th style="width: 5rem;">Roll No</th>
                                <th style="width: 7rem;">Class & Section</th>
                                <th style="width: 5rem;">Date</th>
                                <th>Attendance</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th style="width: 1rem;">#</th>
                                <th style="width: 7rem;">Student</th>
                                <th style="width: 5rem;">Roll No</th>
                                <th style="width: 7rem;">Class & Section</th>
                                <th style="width: 5rem;">Date</th>
                                <th>Attendance</th>
                            </tr>
                        </tfoot>
                        <tbody>

                            @foreach($data['students'] as $key => $student)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $student->student_name }}</td>
                                <td>#{{ $student->student_roll_no }}</td>
                                <td>{{ $student->class->class_title }} & {{ $student->class->section_name }}</td>
                                <td>{{ date('Y-m-d') }}</td>
                                <td>
                                    <input type="hidden" name="student[]" value="{{ $student->id }}">
                                    <input type="hidden" name="class" value="{{ $student->class->id }}">
                                    <select name="attendance[]" class="form-control">
                                        <option value="" selected selected>Choose attendnace</option>
                                        <option value="present">Present</option>
                                        <option value="absent">Absent</option>
                                        <option value="leave">Leave</option>
                                        <option value="late">Late</option>
                                    </select>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                <button class="btn btn-outline-success ml-auto mt-2 btn-mark-attendance">Mark Attendance</button>
            </form>
        </div>
        <div class="card-footer"></div>
    </div>

</div>
@else
{{ $message }}
@endisset
@endsection


@section('script')
<script>
    $(document).ready(function () {
        $("#mark_student_attendance").on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: 'mark_student_attendance',
                method: 'post',
                data: new FormData(this),
                processData: false,
                dataType: "JSON",
                contentType: false,
                cache: false,
                beforeSend: function () {
                    $("#markAttendanceMessage").html('');
                    $("#markAttendanceMessage").removeClass();
                    $(".btn-mark-attendance").html('Prcoessing ...');
                },
                success: function (data) {
                    if (data.response == 1) {
                        $("#markAttendanceMessage").append(data.message);
                        $("#markAttendanceMessage").addClass(data.class);
                        $(".table-responsive").addClass('d-none');
                        $(".btn-mark-attendance").addClass('d-none');
                    }
                    else {
                        $.each(data.errors, function (i, v) {
                            $("#markAttendanceMessage").append(v);
                        });
                        $("#markAttendanceMessage").addClass(data.class);
                        $(".btn-mark-attendance").html('Not Marked');
                    }
                }
            })
        });
    });
</script>
@endsection